// Copyright 2021-2025 Alessio Vanni

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:

// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

var LinkHarvester = (function () {
    'use strict';

    /* The following boilerplate mostly copied from UDN (ex MDN) */
    Components.utils.import('resource://gre/modules/PrivateBrowsingUtils.jsm');
    Components.utils.import('resource://gre/modules/Services.jsm');

    const nsSupportsString = Components.Constructor('@mozilla.org/supports-string;1',
						    'nsISupportsString');
    const String = function (value) {
	let res = nsSupportsString();
	res.data = value;
	return res;
    };

    const nsTransferable = Components.Constructor('@mozilla.org/widget/transferable;1',
						  'nsITransferable');
    const Transferable = function (source) {
	let res = nsTransferable();
	if ('init' in res
	    && source instanceof Components.interfaces.nsIDOMWindow) {
	    res.init(PrivateBrowsingUtils.privacyContextFromWindow(source));
	}
	return res;
    };

    const seek = function (node) {
	let n = node;
	do {
	    let as = n.querySelectorAll('a[href]');
	    if (as && 0 < as.length) {
		return as;
	    }
	    } while ((n = n.parentNode) && (1 == n.nodeType));
	    return [];
	};

    const extend = function (range, backward) {
	let r = range[backward ? 'startContainer' : 'endContainer'];
	if (1 != r.nodeType) {
	    r = r.parentNode;
	}
	let x = seek(r);
	if (0 !== x.length) {
	    let e = backward ? x[0] : x[x.length-1];
	    range[backward ? 'setStartBefore' : 'setEndAfter'](e);
	}
	return range;
    };

    let o = {};

    const fetchLinks = function () {
	let contentWindow = window.gBrowser.selectedBrowser.contentWindow;
	let contentDocument = window.gBrowser.selectedBrowser.contentDocument;

	let selection = contentWindow.getSelection();

	let container = contentDocument.createElement('div');
	let selCount = selection.rangeCount;
	for (let i=0; i<selCount; ++i) {
	    let n = extend(extend(selection.getRangeAt(i), true), true);
	    n = n.cloneContents();
	    container.appendChild(n);
	}

	return container.querySelectorAll('a[href]');
    };

    o.copyLinks = function () {
	let built = '';

	let links = fetchLinks();
	for (let i=0; i<links.length; ++i) {
	    built = built + links[i].href + '\n';
	}

	let t = Transferable(window);
	t.addDataFlavor('text/unicode');
	t.setTransferData('text/unicode', String(built), built.length * 2);

	Services.clipboard.setData(t, null, Services.clipboard.kGlobalClipboard);
    };

    o.openLinks = function () {
	let links = fetchLinks();
	for (let i=0; i<links.length; ++i) {
	    gBrowser.addTab(links[i].href);
	}
    };

    return o;
})();
